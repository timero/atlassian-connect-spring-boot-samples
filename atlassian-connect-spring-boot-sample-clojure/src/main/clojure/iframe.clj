(ns iframe
  (:import (org.springframework.stereotype Controller)
           (org.springframework.web.bind.annotation GetMapping)))

(gen-class
  :name ^{Controller true} addon.IframeController
  :methods [[^{GetMapping "iframe"} iframe [] String]])

(defn -iframe [this] "iframe")
