package sample.connect.spring.dynamodb.dynamo;

import com.atlassian.connect.spring.AtlassianHost;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

@EnableScan
interface DynamoAtlassianHostCrudRepository extends CrudRepository<DynamoAtlassianHost, String> {

    Optional<AtlassianHost> findFirstByBaseUrl(String baseUrl);
}
